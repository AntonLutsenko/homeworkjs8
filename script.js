//Обработчик события это функция, которая отслеживает определенное событие и запускается при наступлении такового.

let containet = document.createElement("div");
containet.classList.add("container");
containet.id = "myList";
document.body.appendChild(containet);

let div = document.createElement("div");
div.classList.add("label");
div.innerText = "Price, UAH: ";
document.body.appendChild(div);

let inputPrice = document.createElement("INPUT");
inputPrice.id = "input";
inputPrice.type = "number";
document.body.querySelector(".label").appendChild(inputPrice);

inputPrice.addEventListener("blur", () => {
	if (inputPrice.value <= 0 || inputPrice.value === "") {
		let list = document.getElementById("myList");
		document
			.querySelector(".label")
			.insertAdjacentHTML(
				"afterend",
				`<span class= "errorMesage">Please enter correct price</span>`
			);
		inputPrice.classList.remove("inputfocus");
		inputPrice.classList.add("errorEnter");
		list.removeChild(list.firstChild);
	} else {
		inputPrice.classList.remove("errorEnter");
		inputPrice.classList.add("input-done");
		if (!containet.hasChildNodes()) {
			myList.insertAdjacentHTML(
				"afterBegin",
				`<span class= "price">Текущая цена: ${inputPrice.value} грн <button class ="remove">x</button></span>`
			);
		} else {
			document.querySelector(
				".price"
			).innerHTML = `Текущая цена: ${inputPrice.value} грн <button class ="remove">x</button>`;
		}
		document.querySelector(".remove").addEventListener("click", function () {
			this.parentNode.remove();
			inputPrice.value = null;
		});
	}
});

inputPrice.addEventListener("focus", () => {
	if (document.querySelector(".errorMesage") != null) {
		document.querySelector(".errorMesage").remove();
	}
	inputPrice.classList.add("inputfocus");
});
